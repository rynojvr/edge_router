package main

import (
	"encoding/binary"
	"fmt"
	"github.com/spf13/viper"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/hariguchi/iproute"
	"github.com/korylprince/ipnetgen"
	"google.golang.org/protobuf/proto"

	"gitlab.com/rynojvr/edge_router/msg"
)

// protoc --go_out=. msg/*.proto

const (
	SrvcPort             = 58080
	DeadCountThreshold   = 3
	DialTimeout          = time.Second * 2
	UpdateDelay          = time.Second * 30
	ConfigKeySendUpdates = "send_updates"
)

var (
	overlayNet = &net.IPNet{
		IP:   net.IPv4(172, 16, 0, 0),
		Mask: net.IPv4Mask(255, 255, 255, 0),
	}
	hostDeadCount      = make(map[string]int, 0)
	hostDeadCountMutex = &sync.RWMutex{}
)

func ToPBFromNet(net *net.IPNet) *msg.Route {
	pbRoute := &msg.Route{
		Dst:      toUint32(net.IP),
		Priority: 404,
	}

	return pbRoute
}

func FromPB(m *msg.Route) (*iproute.Route, error) {
	r := &iproute.Route{
		Priority: int(m.Priority),
		Dst: &net.IPNet{
			IP:   fromUint32(m.Dst),
			Mask: net.IPv4Mask(255, 255, 255, 0),
		},
	}

	return r, nil
}

func getLocalNets() localNets {
	localNets := make(localNets, 0)
	interfaces, _ := net.Interfaces()
	for _, iface := range interfaces {
		addrs, _ := iface.Addrs()
		for _, localAddr := range addrs {
			localNet, _ := localAddr.(*net.IPNet)
			if localNet.IP.IsGlobalUnicast() && localNet.IP.To4() != nil {
				// Set the IP to the network address
				localNet.IP = localNet.IP.Mask(localNet.Mask)
				localNets = append(localNets, localNet)
			}
		}
	}
	return localNets
}

type localNets []*net.IPNet

func (localNets localNets) contains(ip net.IP) bool {
	for _, localNet := range localNets {
		if localNet.IP.Equal(ip) {
			return true
		}
	}
	return false
}

func markHostDead(ip net.IP) {
	hostDeadCountMutex.Lock()
	if deadCount, found := hostDeadCount[ip.String()]; found {
		if deadCount > DeadCountThreshold {
			removeRoutes(ip)
			delete(hostDeadCount, ip.String())
		} else {
			hostDeadCount[ip.String()] = deadCount + 1
		}
	}
	hostDeadCountMutex.Unlock()
}

func removeRoutes(ip net.IP) {
	routes, err := iproute.GetIPv4routes()
	if err != nil {
		log.Fatalln("error retrieving existing routes during route removal:", err.Error())
	}
	for _, route := range routes {
		if route.Gw.Equal(ip) {
			err = iproute.DeleteRoute(&route)
			if err != nil {
				log.Fatalln("error deleting route:", err.Error())
			}
		}
	}
}

func createNewUpdateMsg(nets localNets) *msg.RouteUpdate {
	updateMsg := &msg.RouteUpdate{
		Routes: make([]*msg.Route, 0),
	}
	for _, localNet := range nets {
		m := ToPBFromNet(localNet)
		updateMsg.Routes = append(updateMsg.Routes, m)
	}
	return updateMsg
}

func listenForCleanup() {
	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, os.Interrupt, os.Kill, syscall.SIGKILL, syscall.SIGTERM)
	<-sigChan

	println("Beginning cleanup")
	routes, _ := iproute.GetIPv4routes()
	for _, route := range routes {
		hostDeadCountMutex.RLock()
		if _, found := hostDeadCount[route.Gw.String()]; found {
			println("Removing routes via", route.Gw.String())
			removeRoutes(route.Gw)
		}
		hostDeadCountMutex.RUnlock()
	}
	os.Exit(1)
}

func main() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("/etc/edge-router/")
	viper.SetDefault(ConfigKeySendUpdates, true)
	err := viper.ReadInConfig()
	if err != nil {
		log.Println("Error reading config file:", err.Error())
	}

	go startListener()
	go listenForCleanup()

	if viper.GetBool(ConfigKeySendUpdates) {
		for {
			localNets := getLocalNets()

			gen := ipnetgen.NewFromIPNet(overlayNet)
			for ip := gen.Next(); ip != nil; ip = gen.Next() {
				if localNets.contains(ip) {
					continue
				}
				go func(ip net.IP) {
					destAddr := fmt.Sprintf("%s:%d", ip, SrvcPort)
					con, err := net.DialTimeout("tcp", destAddr, DialTimeout)
					defer func(con net.Conn, ip net.IP) {
						if con != nil {
							err = con.Close()
							if err != nil {
								log.Println("error closing connection:", err.Error())
								markHostDead(ip)
							}
						}
					}(con, ip)
					if err != nil {
						markHostDead(ip)
						return
					}

					println("Sending update to:", destAddr)

					updateMsg := createNewUpdateMsg(localNets)
					msgBytes, err := proto.Marshal(updateMsg)
					_, err = con.Write(msgBytes)

					hostDeadCountMutex.Lock()
					hostDeadCount[ip.String()] = 0
					hostDeadCountMutex.Unlock()
				}(ip)
			}

			time.Sleep(UpdateDelay)
		}
	} else {
		println("Not sending updates. Waiting to receive")
		<-make(chan interface{})
	}
}

func startListener() {
	listenAddr := fmt.Sprintf("0.0.0.0:%d", SrvcPort)
	listener, err := net.Listen("tcp", listenAddr)
	println("Started listener")
	if err != nil {
		log.Fatalln("error starting listener: ", err)
	}
	for {
		conn, err := listener.Accept()
		if err != nil {
			println("Error accepting con:", err)
			continue
		}
		remoteAddr, _ := conn.RemoteAddr().(*net.TCPAddr)
		println("Listener got connection from", remoteAddr.String())

		bytes, err := ioutil.ReadAll(conn)
		if err != nil {
			println("Error reading bytes:", err)
		}

		err = conn.Close()
		if err != nil {
			log.Fatalln("Error closing listener con:", err.Error())
		}

		routeUpdate := &msg.RouteUpdate{}
		err = proto.Unmarshal(bytes, routeUpdate)
		if err != nil {
			log.Fatalln("error unmarshalling update:", err)
		}

		for _, r := range routeUpdate.Routes {
			route, err := FromPB(r)
			if err != nil {
				println("error parsing route", err.Error())
			}

			route.Gw = remoteAddr.IP
			route.Priority += 500

			err = addRoute(route)
			if err != nil {
				println("Error adding route: ", err.Error(), route.String())
			}
		}

		routesFromSrc := getRoutesWithGateway(remoteAddr.IP)
		staleRoutes := make([]iproute.Route, 0)

		for _, routeFromSrc := range routesFromSrc {
			found := false
			for _, r := range routeUpdate.Routes {
				route, _ := FromPB(r)
				if routeFromSrc.Dst.String() == route.Dst.String() {
					found = true
					break
				}
			}
			if !found {
				staleRoutes = append(staleRoutes, routeFromSrc)
			}
		}

		for _, staleRoute := range staleRoutes {
			err = iproute.DeleteRoute(&staleRoute)
			if err != nil {
				log.Printf("cannot delete route: %+v\n", staleRoute)
				log.Println("error deleting route:", err.Error())
			} else {
				log.Printf("deleted route: %+v\n", staleRoute)
			}
		}
	}
}

func addRoute(newRoute *iproute.Route) error {
	routes, err := iproute.GetIPv4routes()
	if err != nil {
		return err
	}
	for _, route := range routes {
		if newRoute.Gw.Equal(route.Gw) && newRoute.Dst.IP.Equal(route.Dst.IP) {
			fmt.Printf("Skipping dup route. Old: %s, New: %s\n",
				route.String(),
				newRoute.String())
			return nil
		}

		if route.Dst != nil && newRoute.Dst.String() == route.Dst.String() && newRoute.Priority >= route.Priority {
			fmt.Printf("Skipping, due to having better route: %+v\n", newRoute)
			return nil
		}
	}
	err = iproute.AddRoute(newRoute)
	if err == nil {
		log.Printf("Added route: %+v", newRoute)
	} else {
		log.Printf("Error adding route [%+v], %s", newRoute, err.Error())
	}
	return err
}

func getRoutesWithGateway(gatewayIP net.IP) []iproute.Route {
	result := make([]iproute.Route, 0)
	routes, _ := iproute.GetIPv4routes()
	for _, route := range routes {
		if route.Gw.Equal(gatewayIP) {
			result = append(result, route)
		}
	}
	return result
}

// gist.github.com/ammario/649d4c0da650162efd404af23e25b86b
func toUint32(ip net.IP) uint32 {
	if len(ip) > 0 {
		result := binary.BigEndian.Uint32(ip.To4())
		return result
	}
	return 0
}

func fromUint32(byteIP uint32) net.IP {
	ip := make(net.IP, 4)
	binary.BigEndian.PutUint32(ip, byteIP)
	return ip
}
