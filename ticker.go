package main

import (
	"math/rand"
	"time"
)

type Ticker struct {
	d time.Duration
	j int

	alive bool

	cancelChan chan struct{}
	signalChan chan struct{}

	C chan time.Time
}

func NewTicker(baseDelay time.Duration, milliJitter int) *Ticker {
	t := Ticker{
		d: baseDelay,
		j: milliJitter,

		alive: true,

		cancelChan: make(chan struct{}),
		signalChan: make(chan struct{}),

		C: make(chan time.Time),
	}
	go t.start()

	return &t
}

func (t *Ticker) Tick() {
	go func() {
		t.C <- time.Now()
	}()
}

func (t *Ticker) start() {
	//t.alive = true

	//for t.alive {
	for {
		delay := t.d

		if t.j > 0 {
			jitter := time.Millisecond * time.Duration(rand.Intn(t.j))
			delay = delay + jitter
		}

		cancel := false
		select {
		case <-time.After(delay):
			t.Tick()
		case <-t.cancelChan:
			//t.alive = false
			cancel = true
		}

		if cancel {
			break
		}
	}

	t.signalChan <- struct{}{}
}

func (t *Ticker) Stop() {
	//if t.alive {
	//      t.alive = false
	t.cancelChan <- struct{}{}
	<-t.signalChan
	//}
}

func (t *Ticker) Reset() {
	t.Stop()
	//t.alive = true

	go t.start()
}
